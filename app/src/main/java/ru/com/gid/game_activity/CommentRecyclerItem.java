package ru.com.gid.game_activity;

import android.util.Log;
import android.widget.TextView;

import androidx.annotation.NonNull;

import com.xwray.groupie.GroupieViewHolder;
import com.xwray.groupie.Item;

import org.jetbrains.annotations.NotNull;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import ru.com.gid.App;
import ru.com.gid.R;
import ru.com.gid.api.CommentModel;
import ru.com.gid.api.CommentPost;
import ru.com.gid.api.UserModel;

public class CommentRecyclerItem extends Item {

    private CommentModel comment;

    public CommentRecyclerItem(CommentModel comment) {
        this.comment = comment;
    }

    @Override
    public void bind(@NonNull GroupieViewHolder viewHolder, int position) {
        ((TextView) viewHolder.itemView.findViewById(R.id.game_comment_content)).setText(comment.getContent());
        ((TextView) viewHolder.itemView.findViewById(R.id.mark_text_view)).setText("Grade: " + Integer.toString(comment.getMark()));
        App.getUserApi().getUserById(App.getToken(), comment.getUserId()).enqueue(new Callback<UserModel>() {
            @Override
            public void onResponse(@NotNull Call<UserModel> call, @NotNull Response<UserModel> response) {
                if (response.isSuccessful()) {
                    ((TextView) viewHolder.itemView.findViewById(R.id.username_text_view))
                            .setText(response.body().getCommonName());
                }
            }

            @Override
            public void onFailure(@NotNull Call<UserModel> call, @NotNull Throwable t) {
                Log.e("UserId", t.getMessage());
            }
        });
    }

    @Override
    public int getLayout() {
        return R.layout.game_comment_recycler_item;
    }
}
