package ru.com.gid.game_activity;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.google.android.material.dialog.MaterialAlertDialogBuilder;
import com.squareup.picasso.Picasso;
import com.xwray.groupie.GroupAdapter;
import com.xwray.groupie.GroupieViewHolder;
import com.xwray.groupie.Item;

import java.io.IOException;
import java.util.List;

import jp.wasabeef.glide.transformations.BlurTransformation;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import ru.com.gid.App;
import ru.com.gid.R;
import ru.com.gid.api.AddGamePostModel;
import ru.com.gid.api.CommentModel;
import ru.com.gid.api.CommentPost;
import ru.com.gid.api.GameModel;

public class GameActivity extends AppCompatActivity {

    RecyclerView videoRecyclerView;
    GameModel model;
    RecyclerView commentRecyclerView;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_game);
        model = (GameModel) getIntent().getSerializableExtra("model");
        videoRecyclerView = findViewById(R.id.game_page_videos_recyclerview);
        GroupAdapter videoAdapter = new GroupAdapter();
        videoRecyclerView.setAdapter(videoAdapter);
        videoRecyclerView.setLayoutManager(new LinearLayoutManager(this,
                RecyclerView.HORIZONTAL, false));

        TextView title = findViewById(R.id.game_title);
        title.setText(model.getName());
        videoAdapter.add(new VideoRecyclerItem(model.getTrailerImage(), model.getTrailer480()));
        TextView about = findViewById(R.id.game_about_content);
        about.setText(model.getAbout());
        ImageView imageView = findViewById(R.id.game_header_imageview);
        Glide.with(this)
                .load(model.getHeaderImage())
                .apply(RequestOptions.bitmapTransform(new BlurTransformation(55)))
                .into(imageView);
        TextView price = findViewById(R.id.game_price_textview);
        if (model.getPriceSet().size() > 0)
            price.setText("Current price is " + model.getPriceSet().get(0).intValue() + " RUB");
        else if (model.getIsFree())
            price.setText("This game is free!");
        else
            price.setText("This game is not yet released and pre-purchase is not yet available");


        RecyclerView screenshotRecyclerView = findViewById(R.id.screenshots_recycler_view);
        GroupAdapter screenshotAdapter = new GroupAdapter();
        screenshotRecyclerView.setAdapter(screenshotAdapter);
        screenshotRecyclerView.setLayoutManager(new LinearLayoutManager(this, RecyclerView.HORIZONTAL, false));
        for (String url : model.getScreenshots()) {
            screenshotAdapter.add(new ScreenshotItem(url));
        }



        commentRecyclerView = findViewById(R.id.game_comment_recyclerview);
        GroupAdapter commentAdapter = new GroupAdapter();
        commentRecyclerView.setAdapter(commentAdapter);
        commentRecyclerView.setLayoutManager(new LinearLayoutManager(this, RecyclerView.VERTICAL, false));
        App.getGameApi().getGameComments(App.getToken(), model.getId()).enqueue(new Callback<List<CommentModel>>() {
            @Override
            public void onResponse(Call<List<CommentModel>> call, Response<List<CommentModel>> response) {
                if (response.isSuccessful())
                    for (CommentModel model :
                            response.body()) {
                        commentAdapter.add(new CommentRecyclerItem(model));
                    }
                else {
                    try {
                        Log.e("response", response.errorBody().string());
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<List<CommentModel>> call, Throwable t) {
                t.printStackTrace();
            }
        });

        Button wishlistButton = findViewById(R.id.add_wishlist_button);
        wishlistButton.setOnClickListener(
                v -> App.getUserApi().addGame(App.getToken(),
                        new AddGamePostModel(model.getId())).
                        enqueue(new Callback<ResponseBody>() {
                            @Override
                            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {

                            }

                            @Override
                            public void onFailure(Call<ResponseBody> call, Throwable t) {
                                t.printStackTrace();
                            }
                        })
        );

        Button postButton = findViewById(R.id.add_comment_button);
        EditText commentContent = findViewById(R.id.comment_edit_text);
        postButton.setOnClickListener(v -> {

            String[] items = new String[]{"1", "2", "3", "4"};
            new MaterialAlertDialogBuilder(GameActivity.this).setTitle("Rate game").setItems(items, (dialog, which) -> {
                CommentPost cm = new CommentPost(commentContent.getText().toString(), which + 1);
                App.getGameApi().rateGame(App.getToken(), model.getId(), cm).enqueue(new Callback<ResponseBody>() {
                    @Override
                    public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                        if (response.isSuccessful()) {
                            App.getGameApi().getGameComments(App.getToken(), model.getId()).enqueue(new Callback<List<CommentModel>>() {
                                @Override
                                public void onResponse(Call<List<CommentModel>> call, Response<List<CommentModel>> response) {
                                    if (response.isSuccessful()) {
                                        GroupAdapter adapter = new GroupAdapter();
                                        for (CommentModel m : response.body()) {
                                            adapter.add(new CommentRecyclerItem(m));
                                        }
                                        commentRecyclerView.setAdapter(adapter);
                                    }
                                }

                                @Override
                                public void onFailure(Call<List<CommentModel>> call, Throwable t) {
                                    Log.e("Comments API", t.getMessage());
                                }
                            });
                        }
                    }

                    @Override
                    public void onFailure(Call<ResponseBody> call, Throwable t) {
                        Log.d("Comment request", t.getMessage());
                    }
                });
            }).show();

        });
    }

    class ScreenshotItem extends Item {

        String url;

        public ScreenshotItem(String bitmapUrl) {
            url = bitmapUrl;
        }

        @Override
        public void bind(@NonNull GroupieViewHolder viewHolder, int position) {
            ImageView image = viewHolder.itemView.findViewById(R.id.screenshot_imageview);
            CardView cw = viewHolder.itemView.findViewById(R.id.screenshot_cardview);
            cw.setPreventCornerOverlap(false);
            Picasso.get().load(url).into(image);
        }

        @Override
        public int getLayout() {
            return R.layout.screenshot_item;
        }
    }
}