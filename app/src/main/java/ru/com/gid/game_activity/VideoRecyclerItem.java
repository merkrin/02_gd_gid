package ru.com.gid.game_activity;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.MediaController;
import android.widget.VideoView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;

import com.bumptech.glide.Glide;
import com.xwray.groupie.GroupieViewHolder;
import com.xwray.groupie.Item;

import ru.com.gid.R;

public class VideoRecyclerItem extends Item {

    String bitmapUrl;
    String videoUrl;

    public VideoRecyclerItem(String bitmapUrl, String videoUrl) {
        this.bitmapUrl = bitmapUrl;
        this.videoUrl = videoUrl;
    }

    @Override
    public void bind(@NonNull GroupieViewHolder viewHolder, int position) {
        ImageView imageView = viewHolder.itemView.findViewById(R.id.game_trailer_video_preview_image);
        CardView cw = viewHolder.itemView.findViewById(R.id.video_cardview);
        cw.setPreventCornerOverlap(false);
        Glide.with(viewHolder.itemView.getContext()).load(bitmapUrl)
                .into(imageView);
        if (videoUrl != null && !videoUrl.isEmpty())
            viewHolder.itemView.findViewById(R.id.play_button).setOnClickListener(v -> {
                Intent httpIntent = new Intent(Intent.ACTION_VIEW);
                httpIntent.setData(Uri.parse(videoUrl));
                viewHolder.itemView.getContext().startActivity(httpIntent);
            });
    }


    @Override
    public int getLayout() {
        return R.layout.game_video_recycler_item;
    }
}
