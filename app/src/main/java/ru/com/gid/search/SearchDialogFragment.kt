package ru.com.gid.search

import android.app.Dialog
import android.content.ContentValues.TAG
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.EditText
import android.widget.ImageButton
import androidx.appcompat.widget.Toolbar
import androidx.fragment.app.DialogFragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.activityViewModels
import com.google.android.material.chip.Chip
import com.google.android.material.chip.ChipGroup
import kotlinx.android.synthetic.main.search_dialog_fragment.*
import ru.com.gid.R
import kotlin.math.min


class SearchDialogFragment : DialogFragment() {

    private lateinit var toolbar: Toolbar
    private val viewModel: SearchViewModel by activityViewModels()
    private val genres: MutableList<String> = mutableListOf()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setStyle(STYLE_NORMAL, R.style.AppTheme_FullScreenDialog);
    }

    companion object {
        fun display(fragmentManager: FragmentManager): SearchDialogFragment {
            val searchDialogFragment = SearchDialogFragment()
            searchDialogFragment.show(fragmentManager, TAG)
            return searchDialogFragment
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        super.onCreateView(inflater, container, savedInstanceState)
        val view: View = inflater.inflate(R.layout.search_dialog_fragment, container, false)

        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        toolbar = view.findViewById(R.id.search_dialog_toolbar)
        val genreButton = view.findViewById<ImageButton>(R.id.add_genre_button)
        val genreChipGroup: ChipGroup = view.findViewById(R.id.genreChipGroup)
        val genreEditText: EditText = view.findViewById(R.id.genreEditText)

        genreButton.setOnClickListener {
            val chip = Chip(requireContext())
            chip.text = genreEditText.text.toString()
            genreEditText.text.clear()
            chip.isCloseIconVisible = true
            chip.isClickable = true
            chip.isCheckable = false
            genreChipGroup.addView(chip as View)
            chip.setOnCloseIconClickListener {
                genreChipGroup.removeView(chip as View)
                genres.remove(chip.text as String)
            }
            genres.add(chip.text as String)
        }
        val minPriceEditText: EditText = view.findViewById(R.id.min_price_edit_text)
        val maxPriceEditText: EditText = view.findViewById(R.id.max_price_edit_text)
        val name: EditText = view.findViewById(R.id.game_name_edit_text)
        toolbar.setNavigationOnClickListener({ v -> dismiss() })
        toolbar.setTitle("Some Title")
        toolbar.inflateMenu(R.menu.search_dialog_menu)
        toolbar.setOnMenuItemClickListener { item ->
            val platforms = mutableListOf<String>()
            if (linux_search_checkbox.isChecked)
                platforms.add("linux")
            if (mac_search_checkbox.isChecked)
                platforms.add("mac")
            if (windows_search_checkbox.isChecked)
                platforms.add("windows")

            viewModel.search(
                name.text.toString(),
                platforms,
                genres,
                if (minPriceEditText.text.isNullOrEmpty() || !minPriceEditText.text.toString()
                        .matches("\\d+".toRegex())
                ) null else minPriceEditText.text.toString().toDouble(),
                if (maxPriceEditText.text.isNullOrEmpty() || !maxPriceEditText.text.toString()
                        .matches("\\d+".toRegex())
                ) null else maxPriceEditText.text.toString()
                    .toDouble()
            )
            dismiss()
            true
        }
    }

    override fun onStart() {
        super.onStart()
        val dialog: Dialog? = dialog
        if (dialog != null) {
            val width = ViewGroup.LayoutParams.MATCH_PARENT
            val height = ViewGroup.LayoutParams.MATCH_PARENT
            dialog.window?.setLayout(width, height)
        }
    }
}