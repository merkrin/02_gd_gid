package ru.com.gid.search

import android.util.Log
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import ru.com.gid.App
import ru.com.gid.api.GameModel
import ru.com.gid.api.SearchModel

class SearchViewModel : ViewModel() {

    val searchResults: MutableLiveData<List<GameModel>> = MutableLiveData()

    fun search(
        name: String,
        platforms: List<String>,
        genres: List<String>,
        minPrice: Double?,
        maxPrice: Double?
    ) {
        App.getGameApi().getGameWithFilters(
            App.getToken(),
            name,
            if (genres.isEmpty()) null else genres.joinToString(",", "{", "}"),
            if (platforms.isEmpty()) null else platforms.joinToString(",", "{", "}"),
            null,
            minPrice,
            maxPrice,
            "descending",
            null,
            100,
            null
        ).enqueue(object : Callback<SearchModel> {
            override fun onFailure(call: Call<SearchModel>, t: Throwable) {
                Log.e("Search", t.message)
            }

            override fun onResponse(
                call: Call<SearchModel>,
                response: Response<SearchModel>
            ) {
                if (response.isSuccessful) {
                    searchResults.value = response.body()?.results
                }
            }

        })
    }

}
