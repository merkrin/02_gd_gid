package ru.com.gid.search

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.search_fragment.*
import ru.com.gid.GamesRVAdapter
import ru.com.gid.R
import ru.com.gid.api.GameModel

class SearchFragment : Fragment() {

    companion object {
        fun newInstance() = SearchFragment()
    }

    private val viewModel: SearchViewModel by activityViewModels()
    private lateinit var recyclerView: RecyclerView
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.search_fragment, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewModel.search(
            "",
            emptyList(),
            emptyList(),
            null,
            null
        )
        val searchButton: Button = view.findViewById(R.id.search_button)
        searchButton.setOnClickListener {
            SearchDialogFragment.display(parentFragmentManager)
        }
        recyclerView = view.findViewById(R.id.search_results_recycler_view)
        viewModel.searchResults.observe(
            viewLifecycleOwner,
            Observer { gameModels: List<GameModel?> ->
                if (!gameModels.isEmpty()) {
                    val adapter = GamesRVAdapter(gameModels)
                    recyclerView.adapter = adapter
                    recyclerView.layoutManager =
                        GridLayoutManager(requireContext(), 2, RecyclerView.VERTICAL, false)
//                    results_text_view.visibility = View.VISIBLE
                }
            })
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

    }

}
