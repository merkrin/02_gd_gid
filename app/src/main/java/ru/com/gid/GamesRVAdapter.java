package ru.com.gid;

import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;

import java.util.List;

import ru.com.gid.api.GameModel;
import ru.com.gid.game_activity.GameActivity;

public class GamesRVAdapter extends RecyclerView.Adapter<GamesRVAdapter.GamesViewHolder> {

    List<GameModel> games;

    public GamesRVAdapter(List<GameModel> games) {
        this.games = games;
    }

    @NonNull
    @Override
    public GamesViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());

        View view = layoutInflater.inflate(R.layout.game_button_layout_recycler_view, parent, false);
        return new GamesViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull GamesViewHolder holder, int position) {
        final GameModel game = games.get(position);
        holder.setGame(game);
    }


    @Override
    public int getItemCount() {
        return games == null ? 0 : games.size();
    }

    public class GamesViewHolder extends RecyclerView.ViewHolder {


        public GamesViewHolder(@NonNull View itemView) {
            super(itemView);
        }

        public void setGame(GameModel model) {
            if (model == null) {
                RecyclerView.LayoutParams param = (RecyclerView.LayoutParams) itemView.getLayoutParams();
                itemView.setVisibility(View.GONE);
                param.height = 0;
                param.width = 0;
                return;
            }
            ImageView button = itemView.findViewById(R.id.game_image);
            CardView cw = itemView.findViewById(R.id.game_button_card_view);
            cw.setPreventCornerOverlap(false);
            Glide.with(itemView.getContext()).load(model.getHeaderImage()).into(button);
            button.setOnClickListener(v -> {
                Intent intent = new Intent(v.getContext(), GameActivity.class);
                intent.putExtra("model", model);
                v.getContext().startActivity(intent);
            });
            LinearLayout iconLayout = itemView.findViewById(R.id.icon_linear_layout);
            LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(
                    LinearLayout.LayoutParams.WRAP_CONTENT,
                    LinearLayout.LayoutParams.WRAP_CONTENT);
            layoutParams.setMargins(5, 10, 10, 10);

            iconLayout.removeAllViews();

            if (model.getPlatforms().contains("windows")) {
                ImageView icon = new ImageView(itemView.getContext());
                icon.setBackgroundResource(R.drawable.ic_windows);
                iconLayout.addView(icon, layoutParams);
            }

            if (model.getPlatforms().contains("mac")) {
                ImageView icon = new ImageView(itemView.getContext());
                icon.setBackgroundResource(R.drawable.ic_mac);
                iconLayout.addView(icon, layoutParams);
            }


            if (model.getPlatforms().contains("linux")) {
                ImageView icon = new ImageView(itemView.getContext());
                icon.setBackgroundResource(R.drawable.ic_linux);
                iconLayout.addView(icon, layoutParams);
            }
        }
    }

}
