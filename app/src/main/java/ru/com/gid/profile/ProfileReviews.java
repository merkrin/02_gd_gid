package ru.com.gid.profile;

import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.xwray.groupie.GroupAdapter;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import ru.com.gid.App;
import ru.com.gid.R;
import ru.com.gid.api.CommentModel;
import ru.com.gid.game_activity.CommentRecyclerItem;

public class ProfileReviews extends Fragment {

    private ProfileViewModel mViewModel;

    public static ProfileReviews newInstance() {
        return new ProfileReviews();
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_profile_reviews, container, false);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mViewModel = ViewModelProviders.of(this).get(ProfileViewModel.class);
        // TODO: Use the ViewModel
        RecyclerView recyclerView = getView().findViewById(R.id.profile_comment_recyclerView);
        GroupAdapter adapter = new GroupAdapter();
        recyclerView.setAdapter(adapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(requireContext(), RecyclerView.VERTICAL, false));
        mViewModel.userComments.observe(getViewLifecycleOwner(), commentModels -> {
            if (!commentModels.isEmpty()) {
                adapter.clear();
                for (CommentModel comment : commentModels) {
                    adapter.add(new CommentRecyclerItem(comment));
                }
            }
        });
        App.getUserApi().getUserComments(App.getToken()).enqueue(new Callback<List<CommentModel>>() {
            @Override
            public void onResponse(Call<List<CommentModel>> call, Response<List<CommentModel>> response) {
                if (response.isSuccessful())
                    mViewModel.postComments(response.body());
            }

            @Override
            public void onFailure(Call<List<CommentModel>> call, Throwable t) {
                Log.e("Personal comments", t.getMessage());
            }
        });
    }

}
