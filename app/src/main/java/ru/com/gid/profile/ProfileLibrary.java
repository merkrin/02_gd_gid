package ru.com.gid.profile;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.xwray.groupie.GroupAdapter;

import ru.com.gid.GameButtonFactory;
import ru.com.gid.GamesRVAdapter;
import ru.com.gid.R;

public class ProfileLibrary extends Fragment {

    private ProfileViewModel mViewModel;
    private RecyclerView recyclerView;
    private GroupAdapter adapter;

    public static ProfileLibrary newInstance() {
        return new ProfileLibrary();
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_profile_library, container, false);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        mViewModel = ViewModelProviders.of(this).get(ProfileViewModel.class);
        // TODO: Use the ViewModel
        GameButtonFactory.getLibraryGames(mViewModel);
        mViewModel.libraryGames.observe(getViewLifecycleOwner(), gameModels -> {
            if (!gameModels.isEmpty()) {
                recyclerView = getActivity().findViewById(R.id.library_recyclerview);

                GamesRVAdapter adapter = new GamesRVAdapter(gameModels);
                recyclerView.setAdapter(adapter);
                recyclerView.setLayoutManager(new GridLayoutManager(getContext(), 2));
            }
        });
    }

}