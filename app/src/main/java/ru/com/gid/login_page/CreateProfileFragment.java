package ru.com.gid.login_page;

import android.content.res.Resources;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.navigation.fragment.NavHostFragment;

import com.google.android.material.bottomnavigation.BottomNavigationView;

import java.util.ArrayList;
import java.util.Arrays;

import ru.com.gid.R;
import ru.com.gid.feed.Feed;
import ru.com.gid.search.SearchFragment;

public class CreateProfileFragment extends Fragment {
    LoginViewModel model;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_create_profile, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {

        model = new ViewModelProvider(this).get(LoginViewModel.class);

        Resources resources = getResources();

        ArrayList<TextView> elements = new ArrayList<>(Arrays.asList(
                view.findViewById(R.id.titleText),
                view.findViewById(R.id.adText),
                view.findViewById(R.id.loginTextButton),
                view.findViewById(R.id.createProfileButton)));

        String[] strings = resources.getStringArray(R.array.login_or_signup_page);

        for (int i = 0; i < elements.size(); i++)
            elements.get(i).setText(strings[i]);


        view.findViewById(R.id.loginTextButton).setOnClickListener(v -> {
            NavHostFragment.findNavController(this).navigate(R.id.action_createProfileFragment_to_loginFragment);
        });

        view.findViewById(R.id.createProfileButton).setOnClickListener(v -> {
            NavHostFragment.findNavController(this).navigate(R.id.action_createProfileFragment_to_profileCreationPageFragment);
        });

        view.findViewById(R.id.skipLogInButton).setOnClickListener(v -> {
            model.skip();
            NavHostFragment.findNavController(this).navigate(R.id.action_createProfileFragment_to_searchFragment);
        });
    }
}
