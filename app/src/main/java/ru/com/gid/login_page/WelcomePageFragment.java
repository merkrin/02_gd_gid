package ru.com.gid.login_page;

import android.content.res.Resources;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.navigation.Navigation;
import androidx.navigation.fragment.NavHostFragment;
import androidx.navigation.ui.NavigationUI;

import java.util.ArrayList;
import java.util.Arrays;

import ru.com.gid.R;

public class WelcomePageFragment extends Fragment {
    private ViewGroup container;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        this.container = container;
        return inflater.inflate(R.layout.fragment_welcome_page, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        Resources resources = getResources();
        ArrayList<TextView> elements = new ArrayList<>(Arrays.asList(
                getView().findViewById(R.id.welcomeText),
                getView().findViewById(R.id.adText)));
        String[] strings = resources.getStringArray(R.array.welcome_page);

        for (int i = 0; i < 2; i++)
            elements.get(i).setText(strings[i]);

        view.findViewById(R.id.continueButton).setOnClickListener(v -> {
            NavHostFragment.findNavController(this).navigate(R.id.action_welcomePageFragment_to_createProfileFragment);
        });
    }
}
