package ru.com.gid.login_page;

import android.content.res.Resources;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.navigation.fragment.NavHostFragment;

import java.util.ArrayList;
import java.util.Arrays;

import ru.com.gid.MainActivity;
import ru.com.gid.R;
import ru.com.gid.api.LoginModel;

public class LoginFragment extends Fragment {
    private ArrayList<EditText> elements;

    private LoginViewModel loginViewModel;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_login, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        loginViewModel = new ViewModelProvider(this).get(LoginViewModel.class);
        Resources resources = getResources();

        elements = new ArrayList<>(Arrays.asList(
                view.findViewById(R.id.editEmail),
                view.findViewById(R.id.editPassword)
        ));

        String[] strings = resources.getStringArray(R.array.login_page);

        ((TextView) (view.findViewById(R.id.loginTitle))).setText(strings[0]);

        for (int i = 0; i < elements.size(); i++)
            elements.get(i).setHint(strings[i + 1]);

        view.findViewById(R.id.continueButton).setOnClickListener(v -> {
            if (elements.get(0).getText().toString().isEmpty() ||
                    elements.get(1).getText().toString().isEmpty()) {
                Toast.makeText(getContext(), "Не все поля заполнены", Toast.LENGTH_SHORT).show();
            } else {
                LoginModel lm = new LoginModel(null, elements.get(0).getText().toString(),
                        null, elements.get(1).getText().toString());
                loginViewModel.authorize(lm);
            }
        });


        final Observer<LoginViewModel.LoginState> loginObserver = state -> {
            if (state == LoginViewModel.LoginState.AUTHORIZED) {
                Log.d("dest", (String) NavHostFragment.findNavController(this).getCurrentDestination().getLabel());
                NavHostFragment.findNavController(this).navigate(R.id.action_loginFragment_to_Feed);
                ((MainActivity) getActivity()).showBottomNav();
            } else if (state == LoginViewModel.LoginState.INVALID_AUTHENTICATION) {
                Toast.makeText(getContext(), "Ошибка авторизации", Toast.LENGTH_SHORT).show();
            }
        };
        loginViewModel.getState().observe(requireActivity(), loginObserver);
    }
}
