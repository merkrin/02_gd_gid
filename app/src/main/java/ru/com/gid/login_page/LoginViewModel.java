package ru.com.gid.login_page;

import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import ru.com.gid.App;
import ru.com.gid.api.LoginModel;
import ru.com.gid.api.TokenResponse;

public class LoginViewModel extends ViewModel {
    private MutableLiveData<LoginState> state = new MutableLiveData<>();

    public MutableLiveData<LoginState> getState() {
        return state;
    }

    public void authorize(LoginModel lm) {
        App.getUserApi().login(lm).enqueue(new Callback<TokenResponse>() {
            @Override
            public void onResponse(Call<TokenResponse> call, Response<TokenResponse> response) {
                if (response.body() != null) {
                    App.setToken(response.body().getToken());
                    state.setValue(LoginState.AUTHORIZED);
                } else {
                    state.setValue(LoginState.INVALID_AUTHENTICATION);
                }
            }

            @Override
            public void onFailure(Call<TokenResponse> call, Throwable t) {
                t.printStackTrace();
                state.setValue(LoginState.UNAUTHORIZED);
            }
        });
    }

    public void register(LoginModel lm) {
        App.getUserApi().register(lm).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                App.getUserApi().login(lm).enqueue(new Callback<TokenResponse>() {
                    @Override
                    public void onResponse(Call<TokenResponse> call, Response<TokenResponse> response) {
                        if (response.body() != null) {
                            App.setToken(response.body().getToken());
                            state.setValue(LoginState.AUTHORIZED);
                        } else {
                            state.setValue(LoginState.INVALID_AUTHENTICATION);
                        }
                    }

                    @Override
                    public void onFailure(Call<TokenResponse> call, Throwable t) {
                        t.printStackTrace();
                        state.setValue(LoginState.UNAUTHORIZED);
                    }
                });
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                t.printStackTrace();
                state.setValue(LoginState.INVALID_AUTHENTICATION);
            }
        });
    }

    public void skip() {
        state.setValue(LoginState.SKIPPED_AUTHENTICATION);
    }

    public enum LoginState {
        AUTHORIZED,
        UNAUTHORIZED,
        INVALID_AUTHENTICATION,
        SKIPPED_AUTHENTICATION
    }

}
