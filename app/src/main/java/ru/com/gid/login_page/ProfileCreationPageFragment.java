package ru.com.gid.login_page;

import android.content.res.Resources;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.navigation.fragment.NavHostFragment;

import java.util.ArrayList;
import java.util.Arrays;

import ru.com.gid.MainActivity;
import ru.com.gid.R;
import ru.com.gid.api.LoginModel;

public class ProfileCreationPageFragment extends Fragment {

    private LoginViewModel loginViewModel;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_profile_creation_page, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        loginViewModel = new ViewModelProvider(this).get(LoginViewModel.class);

        Resources resources = getResources();

        ArrayList<TextView> elements = new ArrayList<>(Arrays.asList(
                view.findViewById(R.id.titleTextView),
                view.findViewById(R.id.emailEditText),
                view.findViewById(R.id.emailHintTextView),
                view.findViewById(R.id.usernameEditText),
                view.findViewById(R.id.usernameHintTextView),
                view.findViewById(R.id.passwordEditText),
                view.findViewById(R.id.passwordHintTextView),
                view.findViewById(R.id.commonNameEditText),
                view.findViewById(R.id.commonNameHintTextView)
        ));

        String[] strings = resources.getStringArray(R.array.signup_page);

        for (int i = 0; i < elements.size(); i++) {
            if (elements.get(i) instanceof EditText)
                (elements.get(i)).setHint(strings[i]);
            else
                (elements.get(i)).setText(strings[i]);
        }

        view.findViewById(R.id.doneButton).setOnClickListener(v -> {
            if (elements.get(1).getText().toString().isEmpty() ||
                    elements.get(3).getText().toString().isEmpty() ||
                    elements.get(7).getText().toString().isEmpty() ||
                    elements.get(5).getText().toString().isEmpty()) {
                Toast.makeText(getContext(), "Не все поля заполнены", Toast.LENGTH_SHORT).show();
            } else {
                LoginModel lm = new LoginModel(elements.get(1).getText().toString(),
                        elements.get(3).getText().toString(), elements.get(7).getText().toString(),
                        elements.get(5).getText().toString());
                loginViewModel.register(lm);
            }
        });

        final Observer<LoginViewModel.LoginState> nameObserver = state -> {
            if (state == LoginViewModel.LoginState.AUTHORIZED) {
                NavHostFragment.findNavController(this).navigate(R.id.action_profileCreationPageFragment_to_Feed);
                ((MainActivity) getActivity()).showBottomNav();
            } else if (state == LoginViewModel.LoginState.INVALID_AUTHENTICATION) {
                Toast.makeText(getContext(), "Ошибка авторизации", Toast.LENGTH_SHORT).show();
            }
        };
        loginViewModel.getState().observe(requireActivity(), nameObserver);
    }
}
