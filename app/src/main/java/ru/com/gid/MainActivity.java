package ru.com.gid;

import android.os.Bundle;
import android.os.PersistableBundle;
import android.view.View;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;

import com.google.android.material.bottomnavigation.BottomNavigationView;

import ru.com.gid.feed.Feed;
import ru.com.gid.login_page.LoginViewModel;
import ru.com.gid.login_page.WelcomePageFragment;
import ru.com.gid.profile.Profile;
import ru.com.gid.search.SearchFragment;

public class MainActivity extends AppCompatActivity {

    LoginViewModel loginViewModel;
    BottomNavigationView navView;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState, @Nullable PersistableBundle persistentState) {
        this.onCreate(savedInstanceState);
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.template_main);

        loginViewModel = new ViewModelProvider(this).get(LoginViewModel.class);
        NavController navController = Navigation.findNavController(this, R.id.nav_host_fragment);

        navView = findViewById(R.id.bottom_navigation_bar);
        hideBottomNav();
        navView.setOnNavigationItemSelectedListener(item -> {
            switch (item.getTitle().toString()) {
                case "Profile":
                    item.setChecked(true);
                    if (navController.getCurrentDestination().getId() == R.id.feed)
                        navController.navigate(R.id.action_feed_to_profile);
                    else if (navController.getCurrentDestination().getId() == R.id.searchFragment)
                        navController.navigate(R.id.action_searchFragment_to_profile);
                    break;
                case "Feed":
                    item.setChecked(true);
                    if (navController.getCurrentDestination().getId() == R.id.searchFragment)
                        navController.navigate(R.id.action_searchFragment_to_feed);
                    else if (navController.getCurrentDestination().getId() == R.id.profile)
                        navController.navigate(R.id.action_profile_to_feed);
                    break;
                case "Search":
                    item.setChecked(true);
                    if (navController.getCurrentDestination().getId() == R.id.feed)
                        navController.navigate(R.id.action_feed_to_searchFragment);
                    else if (navController.getCurrentDestination().getId() == R.id.profile)
                        navController.navigate(R.id.action_profile_to_searchFragment);
                    break;
            }
            return false;
        });

    }

    public void showBottomNav() {
        navView.setVisibility(View.VISIBLE);
    }

    public void hideBottomNav() {
        navView.setVisibility(View.INVISIBLE);
    }
}
