package ru.com.gid.api

data class CommentPost(val content: String, val mark: Int)