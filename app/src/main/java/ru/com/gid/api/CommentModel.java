package ru.com.gid.api;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class CommentModel {

    public CommentModel(String content, int mark, Integer userId, GameModel game) {
        this.content = content;
        this.mark = mark;
        this.userId = userId;
        this.game = game;
    }

    @Expose
    @SerializedName("content")
    private String content;

    @Expose
    @SerializedName("mark")
    private int mark;

    @Expose
    @SerializedName("user_id")
    private Integer userId;

    @Expose
    @SerializedName("game")
    private GameModel game;

    public GameModel getGame() {
        return game;
    }

    public void setGame(GameModel game) {
        this.game = game;
    }

    public int getMark() {
        return mark;
    }

    public void setMark(int mark) {
        this.mark = mark;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }
}
