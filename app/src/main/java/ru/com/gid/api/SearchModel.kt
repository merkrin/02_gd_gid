package ru.com.gid.api

data class SearchModel(val count: Int, val results: List<GameModel>)